import React from 'react';
import {Route, Switch} from 'react-router-dom';
import './App.scss';
import './fonts/Vanila/Fonts/sofiapro-light.otf'

import HomePage from './web/containers/HomePage/HomePage';
import LoginPage from './web/containers/LoginPage/LoginPage';
import RegisterPage from './web/containers/RegisterPage/RegisterPage';

import Navbar from './web/components/layout/header/Navbar/Navbar.js';
import Header from './web/components/layout/header/Header/Header.js';

class App extends React.Component {
  render() {
    return (
      <div>
        <Navbar></Navbar>
        <Header></Header>
        
        <Switch>
          <Route exact path="/" component={HomePage}/>
          <Route exact path="/register" component={RegisterPage}/>
          <Route exact path="/login" component={LoginPage}/>
        </Switch>
      </div>
    );
  }
}

export default App;

