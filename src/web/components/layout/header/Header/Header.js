import React, { Component } from 'react';
import './Header.scss';
import background from '../../../../../images/background/2501059.jpg'

class Header extends Component {
    render() {
        return (
            <div className="back-ground">
                <img src={background}/>
            </div>
        );
    }
}

export default Header;