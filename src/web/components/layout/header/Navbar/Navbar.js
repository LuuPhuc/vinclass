import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './NavbarStyle.scss'

class Navbar extends Component {

    state = {
        isOpen: false
    }

    handleToggle = () => {
        this.setState({isOpen: !this.state.isOpen})
    }
    
    render() {
        return (
            <header>
                <nav className="navbar">
                    <Link  className="nav-logo" to="/">LOGO</Link>
                        <ul className="nav-menu">
                            <li>
                                <Link to="/">Home</Link>
                            </li>
                            <li>
                                <Link to="/login">Login</Link>
                            </li>
                            <li>
                                <Link to="/register">Register</Link>
                            </li>
                        </ul>
                    <Link className="contact-top" to="/">About Us</Link>
                </nav>
            </header>
        );
    }
}

export default Navbar;