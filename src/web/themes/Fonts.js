const size = {
    large: "1.2rem",
    normal: "1rem",
    small: "0.5rem",
}

export default size