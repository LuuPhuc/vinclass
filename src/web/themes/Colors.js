const colors = {
    //white
    white: "#ffffff",

    gray: "#808080",

    //black
    black: "#000000",

    //dark
    dark_font: "#333"
}
export default colors